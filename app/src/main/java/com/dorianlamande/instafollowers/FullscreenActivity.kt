package com.dorianlamande.instafollowers

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import androidx.work.*
import com.dorianlamande.instafollowers.databinding.ActivityFullscreenBinding
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.concurrent.TimeUnit

class MyWorker(context: Context, params: WorkerParameters) : Worker(context, params) {

    override fun doWork(): Result {
        Log.d("MyWorker", "WORK IN PROGRESS")
        executeWork()
        return Result.success()
    }
    private fun executeWork() {
        val repository = InstagramRepository()
        val followers = repository.getFollowersCount()
        MyLiveData.followersCount.postValue(followers)
    }

}

class MyLiveData {
    companion object {
         val followersCount = MutableLiveData<String>()
    }
}

class FullscreenActivity : AppCompatActivity() {

    private lateinit var binding: ActivityFullscreenBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFullscreenBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val work = PeriodicWorkRequestBuilder<MyWorker>(15, TimeUnit.MINUTES).build()
        val workManager = WorkManager.getInstance(this)

        workManager.enqueueUniquePeriodicWork("work", ExistingPeriodicWorkPolicy.REPLACE, work)

        MyLiveData.followersCount.observe(this) {
            binding.instagramFollowersTextView.text =
                    it
            binding.instagramLastRefreshTextView.text =
                    "Mise à jour le ${
                        LocalDateTime.now()
                                .format(DateTimeFormatter.ofPattern("E, dd MMM YYYY à HH:mm:ss"))
                    }"
        }
    }
}