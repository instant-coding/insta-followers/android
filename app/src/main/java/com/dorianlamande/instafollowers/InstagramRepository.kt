package com.dorianlamande.instafollowers

import com.google.gson.Gson
import okhttp3.FormBody
import okhttp3.OkHttpClient
import okhttp3.Request
import java.text.NumberFormat
import java.util.*

class InstagramRepository {

    fun getFollowersCount(): String {
        val client = OkHttpClient()

        val formBody = FormBody.Builder()
            .add("profile", "none")
            .add("username", "juliegri_")
            .add("pipe", "false")
            .add("is_more", "false")
            .add("type", "free")
            .build()

        val request = Request.Builder()
            .url("https://business.notjustanalytics.com/analyze/getProfileInfo")
            .post(formBody)
            .build()

        val response = client.newCall(request).execute()
        val data = Gson().fromJson(response.body?.string(), InstagramAPIModel::class.java)
        response.body?.close()

        return NumberFormat.getInstance(Locale.FRENCH).format(data.follower)
    }
}


data class InstagramAPIModel(
    val follower: Number
)