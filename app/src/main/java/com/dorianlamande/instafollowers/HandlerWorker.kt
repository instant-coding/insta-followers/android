package com.dorianlamande.instafollowers

import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


class HandlerWorker(private val activity: FullscreenActivity) {

    private val followersCount = MutableLiveData<String>()

    fun run() {

        followersCount.observe(activity, { followers ->
            activity.findViewById<TextView>(R.id.instagramFollowersTextView).text =
                followers
            activity.findViewById<TextView>(R.id.instagramLastRefreshTextView).text =
                "Mise à jour le ${
                    LocalDateTime.now()
                        .format(DateTimeFormatter.ofPattern("E, dd MMM YYYY à HH:mm:ss"))
                }"
        })

        CoroutineScope(Dispatchers.IO).launch {
            val repository = InstagramRepository()
            followersCount.postValue(repository.getFollowersCount())
        }
    }
}